import java.util.Scanner;

public class Principal3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		for (int i=0; i<15; i++)
			System.out.println(i);
		
		System.out.println("\n");
		
		boolean valor = true;
		
		for (int i=0; i<5 && valor==true; i++)
			System.out.println(i);
		
		System.out.println("\n");

		int n=0;
		do {
			System.out.println("Hola");
			n++;
		}
		while (n<4);
		
		System.out.println("\n");

		
		// Un menu normalito
		int e=0;
		do {
			System.out.println("Menú normalito");
			System.out.println("--------------");
			System.out.println("\t1.Para sumar");
			System.out.println("\t2.Para restar");
			System.out.println("\t3.Para dividir");
			System.out.println("\t4.Para multiplicar");
			System.out.println("\t5.Para salir");
			System.out.print("Elija una opción: ");
			
			Scanner teclado = new Scanner(System.in);
			e = teclado.nextInt();
			
			switch (e) {
			case 1: System.out.println("Sumar");
					break;
			case 2: System.out.println("Restar");
					break;
			case 3: System.out.println("Dividir");
					break;
			case 4: System.out.println("Multiplicar");
					break;
			case 5: System.out.println("Salir");
					break;
			default: System.out.println("Elección no valida");
					break;
			}
		}
		while (e!=5);
		
		System.out.println("\n");
		
	}

}