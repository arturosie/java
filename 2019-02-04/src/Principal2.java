
import java.util.*;

public class Principal2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Sacar por pantalla los primeros 10 números enteros");
		int n = 0;
		
		while (n<10) {
			System.out.println(n);
			n++;
		}
		
		System.out.println("Sacar por pantalla los 10 primeros numeros pares");
		n = 0;
		while (n <= 20) {
			System.out.println(n);
			n = n+2;
		}
		
		System.out.println("De entre los 20 primeros, multiplos de 7");
		n = 0;
		while (n <= 20) {
			if ((n%3 == 0) || (n%7 == 0)) {
				System.out.println(n);
			}
			n++;
		}
		
		System.out.println("Menu calculadora\n");
		//Si elijo "+" = sumar
		//Si elijo "-" = restar
		//Si elijo "/" = dividir
		//Si elijo "*" = multiplicar
		//Si elijo "@" = salir
		
		System.out.println("Elija opcion: ");
		Scanner teclado = new Scanner(System.in);
		char seleccion = teclado.next().charAt(0);
		
		switch (seleccion) {
			case '+': System.out.println("Sumar");
					break;
			case '-': System.out.println("Restar");
					break;
			case '/': System.out.println("Dividir");
					break;
			case '*': System.out.println("Multiplicar");
					break;
			case '@': System.out.println("Salir");
					break;
			default: System.out.println("Elección no valida");
					break;
		}
	}

}
