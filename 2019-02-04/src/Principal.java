
public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		//Declaración de variables
		
		//Variable entera
		int edad = 20;
		
		//Variable decimal
		double altura = 1.70;
		
		//Variable caracter
		char estadoCivil = 's';
		
		//Variables cadenas
		String nombre = "Arturo Sierra";
		
		//Variable booleana
		boolean madrileño = false;
		
		System.out.println("Tu edad es: "+edad);
		System.out.println(altura);
		System.out.println("\n");
		
		System.out.println("Si eres mayor de 35: \"Eres un viejuno\", si no, \"Puedes jugar\"");
		if (edad > 35) {
			System.out.println("Eres un viejuno\n");
		}
		else {
			System.out.println("Puedes jugar\n");
		}
		
		
		
		System.out.println("Si tienes 35 y mides 1.79, eres JC, si no, no eres JC");
		if (edad == 35 && altura == 1.79) {
			System.out.println("Eres JC\n");
		}
		else {
			System.out.println("No eres JC\n");
		}
		
		
		System.out.println("Si mides mas de 1.70 o no eres de madrid, entras gratis");
		if (altura > 1.70 || madrileño == false) {
			System.out.println("Entras gratis\n");
		}
		else {
			System.out.println("A pagar\n");
		}
		
		
		System.out.println("Si estas casado, mides 1.75 o más y no eres de madrid, Regalo viaje a Punta Cana");
		if (estadoCivil == 'c') {
			if (altura >= 1.75) {
				if (madrileño == false) {
					System.out.println("Viaje a Punta Cana gratis");
				}
				else {
					System.out.println("Lástima de haber nacido en Parla");
				}
			}
			else {
				System.out.println("Por 1 cm nada más!!");
			}
		}
		else {
			System.out.println("Ella dijo NO");
		}
	}


}