
public class Array {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int [] conjunto = {8, 3, 5, 17, 22, 0, 1, 9};
		//Posiciones 	   0  1  2   3   4  5  6  7
		System.out.println(conjunto[4]);
		System.out.println("\n");
		
		//Total de numeros
		System.out.println("El array esta formado por "+conjunto.length+ " numeros");
		System.out.println("\n");
		
		//Sacar el ultimo numero de dos fomras distintas
		System.out.println(conjunto[7]);
		System.out.println("\n");
		//la otra forma
		System.out.println(conjunto[conjunto.length-1]);
		System.out.println("\n");
		
		//Imprimir el array
		for(int i=0; i<conjunto.length; i++)
			System.out.println(conjunto[i]);
		System.out.println("\n");
		
		
// #############################################################################
		
		
		char [] letras = {'g', 't', 'o', 'w', '@', '5', '%', '$', 'F'};
		//Imprimir el array
		for(int i=0; i<letras.length; i++)
			System.out.println(letras[i]);
		System.out.println("\n");
				
				
// #############################################################################
						
								
		String [] palabras = {"casa", "PC", "PeRro", "Audi-A3"};
		
		palabras[2] = "Perro";
		
		//Imprimir el array seguidas y tabuladas
		for(int i=0; i<palabras.length; i++)
			System.out.print(palabras[i]+"\t");
		System.out.println("\n");
		
//#############################################################################
				
				
		
		double [] numeros = {34.7, 2.6, 9.5, 12, 67.14};
		//Imprimir el array al reves
		for(int i=numeros.length-1; i>=0; i--)
			System.out.print(numeros[i]+"\t");
		System.out.println("\n");
		

	}

}
