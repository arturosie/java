
public class Coche {

	//Coche --> Matricula, marca, modelo, precio, tipo, bastidor, ruedas, descapotable

	String matricula;
	String marca;
	String modelo;
	double precio;
	char tipo; // Si es 'G' gasolina, 'D' diesel, 'H' hibrido, 'E' electrico, 'O' otro
	int bastidor;
	int ruedas;
	boolean descapotable;

	//Método constructor
	public Coche(String matricula, String marca, String modelo, double precio, char tipo, int bastidor, int ruedas,
			boolean descapotable) {
		this.matricula = matricula;
		this.marca = marca;
		this.modelo = modelo;
		this.precio = precio;
		this.tipo = tipo;
		this.bastidor = bastidor;
		this.ruedas = ruedas;
		this.descapotable = descapotable;
	}
	//Métodos getters && setters
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	public char getTipo() {
		return tipo;
	}
	public void setTipo(char tipo) {
		this.tipo = tipo;
	}
	public int getBastidor() {
		return bastidor;
	}
	public void setBastidor(int bastidor) {
		this.bastidor = bastidor;
	}
	public int getRuedas() {
		return ruedas;
	}
	public void setRuedas(int ruedas) {
		this.ruedas = ruedas;
	}
	public boolean isDescapotable() {
		return descapotable;
	}
	public void setDescapotable(boolean descapotable) {
		this.descapotable = descapotable;
	}

	//Método TOSTRING
	@Override
	public String toString() {
		return "Coche [matricula=" + matricula + ", marca=" + marca + ", modelo=" + modelo + ", precio=" + precio
				+ ", tipo=" + tipo + ", bastidor=" + bastidor + ", ruedas=" + ruedas + ", descapotable=" + descapotable
				+ "]";
	}

        // Metodo calcular precio del coche con IVA
        public double calculaIVA(){
            return precio*1.21;
        }

        public char categoria(){
            char cat = ' ';

            if (tipo == 'D')
                cat = 'B';
            if (tipo == 'G')
                cat = 'C';
            if (tipo == 'H')
                cat = 'A';
            return cat;
        }

        public void pincharRueda(){
            ruedas = getRuedas()-1;
        }



}
