
public class Principal2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Coche coche1 = new Coche("1234CHV", "Volvo", "S60", 25000, 'D', 123, 5, false);
		Coche coche2 = new Coche("1289KZD", "Mercedes-Benz", "CLA", 50000, 'G', 344, 5, true);
		Coche coche3 = new Coche("4214GHV", "Kia", "Sportage", 20000, 'G', 520, 4, false);
		Coche coche4 = new Coche("0034FHF", "Opel", "Astra", 26000, 'H', 277, 5, false);
		Coche coche5 = new Coche("2995HHF", "Volvo", "FH16", 100000, 'D', 840, 6, true);
		Coche coche6 = new Coche("4000FGK", "Citroen", "Berlingo", 13000, 'G', 631, 5, false);
		Coche coche7 = new Coche("0034KKK", "Suzuki", "Ninja", 48000, 'G', 559, 2, true);
		Coche coche8 = new Coche("7740KCC", "Renault", "Clio", 16500, 'D', 215, 4, false);

                Coche[][] arraydeCoches = new Coche[2][4];

                arraydeCoches[1][1] = coche1;
                arraydeCoches[0][3] = coche2;
                arraydeCoches[0][0] = coche3;
                arraydeCoches[1][2] = coche4;
                arraydeCoches[0][1] = coche5;
                arraydeCoches[1][0] = coche6;
                arraydeCoches[0][2] = coche7;
                arraydeCoches[1][3] = coche8;

                //Imprimimos la marca del arraydeCoches
                for (int fila = 0; fila < arraydeCoches.length; fila++) {
                    for (int col = 0; col < arraydeCoches[fila].length; col++)
                        System.out.print(arraydeCoches[fila][col].getMarca()+ "\t");
                    System.out.print("\n");
                }

                //Calcular IVA del coche
                System.out.println("\nSaber precio llamando a calculaIVA (precio con IVA)\n");
                System.out.println("El precio sin IVA es "+ coche1.getPrecio()+" y con IVA "+coche1.calculaIVA()+"\n");

                //Calcular la categoria del coche
                System.out.println("La categoria del coche es "+coche1.categoria());

                //Calcular ruedas despues de pinchar
                coche1.pincharRueda(); //Pedimos el dato del metodo pincharRueda
                System.out.println("El numero de ruedas del coche es "+coche1.getRuedas());

                //Calcular ruedas despues de pinchar otra vez
                coche1.pincharRueda();
                System.out.println("Ahora tiene "+coche1.getRuedas()+" ruedas");

        }

}
