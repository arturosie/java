
public class AlumnoObjeto {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Creacion de un objteo Alumno

		Alumno alumno1 = new Alumno("09112093H", "Manolo", "Perez", 1, 85, 'M', false);
		Alumno alumno2 = new Alumno("09121354G", "Rosa", "Fernandez", 1, 55, 'F', true);
		Alumno alumno3 = new Alumno("12345678Z", "Jose Ugenio", "Sanavia", 1, 93, 'M', true);

		System.out.println(alumno2);
		System.out.println("Nombre " + alumno1.getNombre() + " " + alumno1.getApellido());

		Equipo equipo1 = new Equipo("FC Barcelona", 27, 119, 20000000, true, 'F');
		Equipo equipo2 = new Equipo("Manchester City", 28, 124, 350000000, true, 'F');

		System.out.println(equipo1);
		System.out.println("Nombre " + equipo2.getNombre() + " " + equipo2.getPlantilla() + " años");

		Alumno[] arrayDeAlumno = { alumno1, alumno2, alumno3 };
		System.out.println("Array de alumnos");
		for (int i = arrayDeAlumno.length - 1; i >= 0; i--) {
			System.out.println(arrayDeAlumno[i]);
		}

		System.out.println("Matriz");

		int[][] matriz = { { 1, 3, 0, -2, 7 }, 
						   { 4, 9, 9, 5, 6 }, 
						   { 8, 1, 3, 4, 5 } };

		for (int fila = 0; fila < matriz.length; fila++) {
			for (int col = 0; col < matriz[fila].length; col++)
				System.out.print(matriz[fila][col] + "\t");
			System.out.print("\n");
		}
	}
}
