
public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Creación de un objeto coche
		
		Coche coche1 = new Coche("123DFW","Audi","A3",21340.65,'D',123459876,5,false);
		Coche coche2 = new Coche("563JDA","Ford","Focus",18225.82,'G',123569874,5,true);
		
		System.out.println(coche1);
		System.out.println(coche2.getPrecio());
		System.out.println(coche1.getTipo());
		
		coche1.setPrecio(15000);
		System.out.println(coche1.getPrecio());
		
		System.out.println("Array de coches");
		
		int [] array = {1, 3, 4, 7, 9, -2, 0, 7};
		
		Coche [] arrayDeCoches = {coche1, coche2};
		
		for(int i=0; i< arrayDeCoches.length; i++) {
			System.out.println(arrayDeCoches[i]);
		}
	}

}
