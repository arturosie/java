import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.border.LineBorder;
import java.awt.Font;
import java.awt.Toolkit;

public class acerca_de_nosotros {

	private JFrame frmAcercaDeNosotros;
	acerca_de_nosotros irAcercaDeNosotros;

	public JFrame getFrame() {
		return frmAcercaDeNosotros;
	}

	public void setFrame(JFrame frame) {
		this.frmAcercaDeNosotros = frame;
	}

	public acerca_de_nosotros getIrAcercaDeNosotros() {
		return irAcercaDeNosotros;
	}

	public void setIrAcercaDeNosotros(acerca_de_nosotros irAcercaDeNosotros) {
		this.irAcercaDeNosotros = irAcercaDeNosotros;
	}

	public acerca_de_nosotros() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmAcercaDeNosotros = new JFrame();
		frmAcercaDeNosotros.getContentPane().setBackground(Color.WHITE);
		frmAcercaDeNosotros.setBackground(Color.WHITE);
		frmAcercaDeNosotros.setTitle("WRITER");
		frmAcercaDeNosotros.setIconImage(Toolkit.getDefaultToolkit().getImage("img\\logo.PNG"));
		frmAcercaDeNosotros.setBounds(100, 100, 540, 373);
		frmAcercaDeNosotros.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmAcercaDeNosotros.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Creador: ");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel.setBounds(124, 90, 73, 20);
		frmAcercaDeNosotros.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Arturo Sierra S\u00E1nchez");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_1.setBounds(207, 91, 163, 19);
		frmAcercaDeNosotros.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Fecha:");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel_2.setBounds(124, 121, 64, 14);
		frmAcercaDeNosotros.getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("01/10/2019");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_3.setBounds(207, 120, 163, 14);
		frmAcercaDeNosotros.getContentPane().add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Copyright \u00A9 | WRITER COMPANY. Todos los derechos reservados.");
		lblNewLabel_4.setBounds(67, 279, 383, 14);
		frmAcercaDeNosotros.getContentPane().add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Versi\u00F3n:");
		lblNewLabel_5.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel_5.setBounds(124, 145, 64, 14);
		frmAcercaDeNosotros.getContentPane().add(lblNewLabel_5);
		
		JLabel lblNewLabel_6 = new JLabel("1.0.0");
		lblNewLabel_6.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel_6.setBounds(207, 145, 163, 14);
		frmAcercaDeNosotros.getContentPane().add(lblNewLabel_6);
		
		JLabel lblCuadro = new JLabel("");
		lblCuadro.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCuadro.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		lblCuadro.setBounds(94, 42, 323, 215);
		frmAcercaDeNosotros.getContentPane().add(lblCuadro);
		
		JLabel lblNewLabel_7 = new JLabel("");
		lblNewLabel_7.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblNewLabel_7.setBounds(103, 53, 303, 192);
		frmAcercaDeNosotros.getContentPane().add(lblNewLabel_7);
	}

}
