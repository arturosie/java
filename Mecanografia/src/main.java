import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;

import javax.swing.JProgressBar;
import java.awt.Toolkit;

public class main {

	private JFrame frmMain;
	int i = 0;
	Timer timer;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					main window = new main();
					window.frmMain.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public main() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMain = new JFrame();
		frmMain.setIconImage(Toolkit.getDefaultToolkit().getImage("img\\logo.PNG"));
		frmMain.getContentPane().setBackground(Color.WHITE);
		frmMain.getContentPane().setLayout(null);

		Image img= new ImageIcon("img\\logo.PNG").getImage();
		ImageIcon img2=new ImageIcon(img.getScaledInstance(400, 200, Image.SCALE_SMOOTH));

		JLabel lblLogo = new JLabel("");		
		lblLogo.setIcon(img2);
		lblLogo.setBounds(310, 54, 503, 251);
		frmMain.getContentPane().add(lblLogo);

		JProgressBar progressBar = new JProgressBar();
		progressBar.setStringPainted(true);
		progressBar.setBounds(310, 367, 503, 14);
		frmMain.getContentPane().add(progressBar);
		frmMain.setTitle("WRITER");
		frmMain.setResizable(false);
		frmMain.setBounds(100, 100, 1100, 616);
		frmMain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Aqu� se pone en marcha el timer cada segundo.
		timer = new Timer(10, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				i++;
				progressBar.setValue(i + 1);
				if(i == 100) {
					timer.stop();
					login cargar = new login();
					cargar.getFrame().setVisible(true);
					frmMain.dispose();
				}

			}
		});
		
		timer.start();
		
	}
}
