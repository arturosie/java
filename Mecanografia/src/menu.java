import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.Color;
import java.awt.Font;
import javax.swing.border.LineBorder;
import javax.swing.JTextPane;
import javax.swing.JScrollPane;

public class menu {

	private JFrame frameMenu;
	JTextPane textPane = new JTextPane();

	
	String Leccion = "";
	String usuario = "anonimo";

	/**
	 * Create the application.
	 */
	public menu(String Usuario) {
		usuario = Usuario;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frameMenu = new JFrame();
		frameMenu.setTitle("WRITER");
		frameMenu.setIconImage(Toolkit.getDefaultToolkit().getImage("img\\logo.PNG"));
		frameMenu.getContentPane().setBackground(Color.WHITE);
		frameMenu.getContentPane().setLayout(null);
		frameMenu.setBounds(100, 100, 1100, 616);
		frameMenu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Image img= new ImageIcon("img\\logo.PNG").getImage();
		ImageIcon img2=new ImageIcon(img.getScaledInstance(400, 200, Image.SCALE_SMOOTH));

		JLabel lblLogo = new JLabel("");		
		lblLogo.setIcon(img2);
		lblLogo.setBounds(299, 55, 503, 251);
		frameMenu.getContentPane().add(lblLogo);
		
		JButton btnLeccion1 = new JButton("LECCI\u00D3N 1");
		btnLeccion1.setBackground(Color.ORANGE);
		btnLeccion1.setFocusPainted(false);
		btnLeccion1.setBounds(283, 317, 120, 34);
		frameMenu.getContentPane().add(btnLeccion1);
		
		JButton btnLeccion2 = new JButton("LECCI\u00D3N 2");
		btnLeccion2.setFocusPainted(false);
		btnLeccion2.setBackground(Color.ORANGE);
		btnLeccion2.setBounds(644, 316, 120, 34);
		frameMenu.getContentPane().add(btnLeccion2);
		
		JLabel lblNewLabel = new JLabel("Hola " + usuario + ", revisa tus estadisticas.");
		lblNewLabel.setFont(new Font("Tahoma", Font.ITALIC, 12));
		lblNewLabel.setBounds(366, 380, 225, 48);
		frameMenu.getContentPane().add(lblNewLabel);
		
		JButton btnRevisar = new JButton("Revisar");
		btnRevisar.setFocusPainted(false);
		btnRevisar.setBackground(Color.ORANGE);
		btnRevisar.setBounds(445, 419, 96, 22);
		frameMenu.getContentPane().add(btnRevisar);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(89, 63, 884, 415);
		frameMenu.getContentPane().add(scrollPane_1);
		scrollPane_1.setViewportView(textPane);
		
		textPane.setBorder(new LineBorder(new Color(0, 0, 0)));
		textPane.setVisible(false);
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.setVisible(false);
		btnVolver.setFocusPainted(false);
		btnVolver.setBackground(Color.ORANGE);
		btnVolver.setBounds(445, 501, 96, 22);
		frameMenu.getContentPane().add(btnVolver);
		
		btnLeccion1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				leer_archivo leer = new leer_archivo();
				try {
					Leccion = leer.muestraContenido("file\\leccion.txt", "Leccion 1");
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				practica cargar = new practica(Leccion, "Leccion 1", usuario);
				cargar.getFrmPractica().setVisible(true);
				frameMenu.dispose();
			}
		});
		
		btnLeccion2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				leer_archivo leer = new leer_archivo();
				try {
					Leccion = leer.muestraContenido("file\\leccion.txt", "Leccion 2");
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				practica cargar = new practica(Leccion, "Leccion 2", usuario);
				cargar.getFrmPractica().setVisible(true);
				frameMenu.dispose();
			}
		});
		
		btnRevisar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String texto;
				
				lblNewLabel.setVisible(false);
				btnLeccion1.setVisible(false);
				btnLeccion2.setVisible(false);
				lblLogo.setVisible(false);
				btnRevisar.setVisible(false);
				textPane.setVisible(true);
				btnVolver.setVisible(true);
				String archivo = "file\\" + usuario + "estadisticas.txt";
				try {
					texto = muestraContenido(archivo);
					textPane.setText(texto);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblNewLabel.setVisible(true);
				btnLeccion1.setVisible(true);
				btnLeccion2.setVisible(true);
				lblLogo.setVisible(true);
				btnRevisar.setVisible(true);
				textPane.setVisible(false);
				btnVolver.setVisible(false);
			}
		});
	}
	
	public String muestraContenido(String archivo) throws FileNotFoundException, IOException {
        String cadena = "", 
        		practica = "";
        BufferedReader b = new BufferedReader(new InputStreamReader(new FileInputStream(archivo), "utf-8"));
        while((cadena = b.readLine())!=null) {
        	practica += cadena + "\n";
        }
        b.close();
                
        return practica;        
    }

	public JFrame getFrameMenu() {
		return frameMenu;
	}

	public void setFrameMenu(JFrame frameMenu) {
		this.frameMenu = frameMenu;
	}
}
