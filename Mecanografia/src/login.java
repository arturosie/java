import java.awt.Font;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileNotFoundException;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.SystemColor;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JPasswordField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class login {

	private JFrame frmLogin;
	private JTextField textFieldUsuario;
	private JPasswordField passwordField;
	
	String usuario = "anonimo";
	
	public JFrame getFrame() {
		return frmLogin;
	}

	public void setFrame(JFrame frame) {
		this.frmLogin = frame;
	}


	
	public login() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmLogin = new JFrame();
		frmLogin.setTitle("WRITER");
		frmLogin.setIconImage(Toolkit.getDefaultToolkit().getImage("img\\logo.PNG"));
		frmLogin.getContentPane().setBackground(Color.WHITE);
		frmLogin.setBounds(100, 100, 1100, 616);
		frmLogin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmLogin.getContentPane().setLayout(null);
		
		Image img= new ImageIcon("img\\logo.PNG").getImage();
		ImageIcon img2=new ImageIcon(img.getScaledInstance(300, 100, Image.SCALE_SMOOTH));
		
		JLabel lblLogo = new JLabel("");		
		lblLogo.setBorder(null);
		lblLogo.setIcon(img2);
		lblLogo.setBounds(356, 99, 396, 140);
		frmLogin.getContentPane().add(lblLogo);
		
		JLabel lblAcercaDe = new JLabel("Acerca de nosotros");
		lblAcercaDe.setForeground(SystemColor.textHighlight);
		lblAcercaDe.setFont(new Font("Berlin Sans FB", Font.PLAIN, 13));
		lblAcercaDe.setBounds(939, 11, 145, 28);
		frmLogin.getContentPane().add(lblAcercaDe);
		
		JLabel lblUsuario = new JLabel("USUARIO: ");
		lblUsuario.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblUsuario.setBounds(358, 250, 86, 28);
		frmLogin.getContentPane().add(lblUsuario);
		
		textFieldUsuario = new JTextField();
		textFieldUsuario.setBounds(443, 256, 215, 20);
		frmLogin.getContentPane().add(textFieldUsuario);
		textFieldUsuario.setColumns(10);
		
		JLabel lblContrasea = new JLabel("CONTRASE\u00D1A:");
		lblContrasea.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblContrasea.setBounds(327, 280, 117, 20);
		frmLogin.getContentPane().add(lblContrasea);
		
		JButton btnAcceder = new JButton("Acceder");
		btnAcceder.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnAcceder.setFocusPainted(false);
		btnAcceder.setBorderPainted(false);
		btnAcceder.setBorder(null);
		btnAcceder.setBackground(new Color(255, 140, 0));
		btnAcceder.setBounds(401, 343, 196, 34);
		frmLogin.getContentPane().add(btnAcceder);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(443, 282, 215, 20);
		frmLogin.getContentPane().add(passwordField);
		frmLogin.setTitle("WRITER");
		frmLogin.setResizable(false);
		frmLogin.setBounds(100, 100, 1100, 616);
		frmLogin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		lblAcercaDe.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				acerca_de_nosotros cargar = new acerca_de_nosotros();
				cargar.getFrame().setVisible(true);
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				lblAcercaDe.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				lblAcercaDe.setFont(new Font("Berlin Sans FB", Font.PLAIN, 13));
			}
		});
		
		textFieldUsuario.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER)
					Login();
			}
		});
		
		passwordField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER)
					Login();
			}
		});
		
		btnAcceder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Login();
			}
		});

	}
	
	public void Login () {
		boolean login = false;
		String pass= String.valueOf(passwordField.getPassword());
		usuario = textFieldUsuario.getText();
		
		try {
			leer_archivo leer = new leer_archivo();
			login = leer.encuentra("file\\log.txt", pass, usuario, login);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} /*catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		
		if (login) {
			menu cargar = new menu(usuario);
			cargar.getFrameMenu().setVisible(true);
			frmLogin.dispose();
			
		}else 
			JOptionPane.showMessageDialog(null, "El nombre o la contrase\u00F1a son incorrectas", "Error de login", JOptionPane.ERROR_MESSAGE);
		
	}
}
