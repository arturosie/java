import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.Component;

import javax.swing.border.LineBorder;
import javax.swing.JTextPane;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Date;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.SystemColor;

public class practica {

	private JFrame frmPractica;

	JLabel lblContador = new JLabel("00:00");
	JLabel lblPTotalesRes = new JLabel("0");
	JLabel lblPPMRes = new JLabel("0");
	JLabel lblErroresRes = new JLabel("0");
	JButton btnReintentarLeccion = new JButton("Reintentar lecci\u00F3n");
	JButton btnVolverMenu = new JButton("Volver al menu");

	JLabel label = null;
	JLabel labelaux = null;
	String Leccion;
	String NumeroLeccion;
	String pTotales, ppm;
	String Usuario;
	
	java.util.Date fecha = new Date();
	
	char[] aCaracteres;
	int sec = 0, min = 0, contadorletras = 0, errores = 0;
	boolean start = false, stoplec = false;
	Timer timer;

	public JFrame getFrmPractica() {
		return frmPractica;
	}

	public void setFrmPractica(JFrame frmPractica) {
		this.frmPractica = frmPractica;
	}

	public KeyListener Listener = new KeyListener() {

		@Override
		public void keyTyped(KeyEvent e) {
			// TODO Auto-generated method stub
			// Resaltar(e.getKeyChar() );
		}

		@Override
		public void keyReleased(KeyEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void keyPressed(KeyEvent e) {
			// TODO Auto-generated method stub

		}
	};

	public practica(String leccion, String NLeccion, String usuario) {
		Leccion = leccion;
		NumeroLeccion = NLeccion;
		Usuario = usuario;
		aCaracteres = Leccion.toCharArray();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmPractica = new JFrame();
		frmPractica.setResizable(false);
		frmPractica.getContentPane().setBackground(Color.WHITE);
		frmPractica.setTitle("WRITER");
		frmPractica.setIconImage(Toolkit.getDefaultToolkit().getImage("img\\logo.PNG"));
		frmPractica.setBounds(100, 100, 1935, 1050);
		frmPractica.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frmPractica.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmPractica.getContentPane().setLayout(null);

		JTextPane textPane_1 = new JTextPane();
		textPane_1.setEditable(false);
		textPane_1.setText(Leccion);
		textPane_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		textPane_1.setBounds(207, 131, 1089, 140);
		frmPractica.getContentPane().add(textPane_1);

		JTextPane textPane = new JTextPane();
		textPane.setBorder(new LineBorder(new Color(0, 0, 0)));
		textPane.setBounds(207, 303, 1089, 167);
		textPane.addKeyListener(Listener);
		frmPractica.getContentPane().add(textPane);

		JLabel lblQ = new JLabel("Q");
		lblQ.setBackground(SystemColor.control);
		lblQ.setOpaque(true);
		lblQ.setHorizontalAlignment(SwingConstants.CENTER);
		lblQ.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblQ.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblQ.setBounds(336, 605, 80, 80);
		frmPractica.getContentPane().add(lblQ);

		JLabel lblW = new JLabel("W");
		lblW.setOpaque(true);
		lblW.setHorizontalAlignment(SwingConstants.CENTER);
		lblW.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblW.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblW.setBounds(426, 605, 80, 80);
		frmPractica.getContentPane().add(lblW);

		JLabel lblE = new JLabel("E");
		lblE.setOpaque(true);
		lblE.setHorizontalAlignment(SwingConstants.CENTER);
		lblE.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblE.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblE.setBounds(516, 605, 80, 80);
		frmPractica.getContentPane().add(lblE);

		JLabel lblR = new JLabel("R");
		lblR.setOpaque(true);
		lblR.setHorizontalAlignment(SwingConstants.CENTER);
		lblR.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblR.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblR.setBounds(607, 605, 80, 80);
		frmPractica.getContentPane().add(lblR);

		JLabel lblT = new JLabel("T");
		lblT.setOpaque(true);
		lblT.setHorizontalAlignment(SwingConstants.CENTER);
		lblT.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblT.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblT.setBounds(697, 605, 80, 80);
		frmPractica.getContentPane().add(lblT);

		JLabel lblY = new JLabel("Y");
		lblY.setOpaque(true);
		lblY.setHorizontalAlignment(SwingConstants.CENTER);
		lblY.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblY.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblY.setBounds(787, 605, 80, 80);
		frmPractica.getContentPane().add(lblY);

		JLabel lblU = new JLabel("U");
		lblU.setOpaque(true);
		lblU.setHorizontalAlignment(SwingConstants.CENTER);
		lblU.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblU.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblU.setBounds(877, 605, 80, 80);
		frmPractica.getContentPane().add(lblU);

		JLabel lblI = new JLabel("I");
		lblI.setOpaque(true);
		lblI.setHorizontalAlignment(SwingConstants.CENTER);
		lblI.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblI.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblI.setBounds(967, 605, 80, 80);
		frmPractica.getContentPane().add(lblI);

		JLabel lblO = new JLabel("O");
		lblO.setOpaque(true);
		lblO.setHorizontalAlignment(SwingConstants.CENTER);
		lblO.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblO.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblO.setBounds(1057, 605, 80, 80);
		frmPractica.getContentPane().add(lblO);

		JLabel lblP = new JLabel("P");
		lblP.setOpaque(true);
		lblP.setHorizontalAlignment(SwingConstants.CENTER);
		lblP.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblP.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblP.setBounds(1146, 605, 80, 80);
		frmPractica.getContentPane().add(lblP);

		JLabel lblA = new JLabel("A");
		lblA.setOpaque(true);
		lblA.setHorizontalAlignment(SwingConstants.CENTER);
		lblA.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblA.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblA.setBounds(346, 696, 80, 80);
		frmPractica.getContentPane().add(lblA);

		JLabel lblS = new JLabel("S");
		lblS.setOpaque(true);
		lblS.setHorizontalAlignment(SwingConstants.CENTER);
		lblS.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblS.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblS.setBounds(436, 696, 80, 80);
		frmPractica.getContentPane().add(lblS);

		JLabel lblD = new JLabel("D");
		lblD.setOpaque(true);
		lblD.setHorizontalAlignment(SwingConstants.CENTER);
		lblD.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblD.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblD.setBounds(526, 696, 80, 80);
		frmPractica.getContentPane().add(lblD);

		JLabel lblF = new JLabel("F");
		lblF.setOpaque(true);
		lblF.setHorizontalAlignment(SwingConstants.CENTER);
		lblF.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblF.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblF.setBounds(617, 696, 80, 80);
		frmPractica.getContentPane().add(lblF);

		JLabel lblG = new JLabel("G");
		lblG.setOpaque(true);
		lblG.setHorizontalAlignment(SwingConstants.CENTER);
		lblG.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblG.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblG.setBounds(707, 696, 80, 80);
		frmPractica.getContentPane().add(lblG);

		JLabel lblH = new JLabel("H");
		lblH.setOpaque(true);
		lblH.setHorizontalAlignment(SwingConstants.CENTER);
		lblH.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblH.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblH.setBounds(797, 696, 80, 80);
		frmPractica.getContentPane().add(lblH);

		JLabel lblJ = new JLabel("J");
		lblJ.setOpaque(true);
		lblJ.setHorizontalAlignment(SwingConstants.CENTER);
		lblJ.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblJ.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblJ.setBounds(887, 696, 80, 80);
		frmPractica.getContentPane().add(lblJ);

		JLabel lblK = new JLabel("K");
		lblK.setOpaque(true);
		lblK.setHorizontalAlignment(SwingConstants.CENTER);
		lblK.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblK.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblK.setBounds(977, 696, 80, 80);
		frmPractica.getContentPane().add(lblK);

		JLabel lblL = new JLabel("L");
		lblL.setOpaque(true);
		lblL.setHorizontalAlignment(SwingConstants.CENTER);
		lblL.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblL.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblL.setBounds(1067, 696, 80, 80);
		frmPractica.getContentPane().add(lblL);

		JLabel label_10 = new JLabel("\u00D1");
		label_10.setOpaque(true);
		label_10.setHorizontalAlignment(SwingConstants.CENTER);
		label_10.setFont(new Font("Tahoma", Font.BOLD, 20));
		label_10.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_10.setBounds(1156, 696, 80, 80);
		frmPractica.getContentPane().add(label_10);

		JLabel lblZ = new JLabel("Z");
		lblZ.setOpaque(true);
		lblZ.setHorizontalAlignment(SwingConstants.CENTER);
		lblZ.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblZ.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblZ.setBounds(382, 786, 80, 80);
		frmPractica.getContentPane().add(lblZ);

		JLabel lblX = new JLabel("X");
		lblX.setOpaque(true);
		lblX.setHorizontalAlignment(SwingConstants.CENTER);
		lblX.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblX.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblX.setBounds(472, 786, 80, 80);
		frmPractica.getContentPane().add(lblX);

		JLabel lblC = new JLabel("C");
		lblC.setOpaque(true);
		lblC.setHorizontalAlignment(SwingConstants.CENTER);
		lblC.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblC.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblC.setBounds(562, 786, 80, 80);
		frmPractica.getContentPane().add(lblC);

		JLabel lblV = new JLabel("V");
		lblV.setOpaque(true);
		lblV.setHorizontalAlignment(SwingConstants.CENTER);
		lblV.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblV.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblV.setBounds(653, 786, 80, 80);
		frmPractica.getContentPane().add(lblV);

		JLabel lblB = new JLabel("B");
		lblB.setOpaque(true);
		lblB.setHorizontalAlignment(SwingConstants.CENTER);
		lblB.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblB.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblB.setBounds(743, 786, 80, 80);
		frmPractica.getContentPane().add(lblB);

		JLabel lblN = new JLabel("N");
		lblN.setOpaque(true);
		lblN.setHorizontalAlignment(SwingConstants.CENTER);
		lblN.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblN.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblN.setBounds(833, 786, 80, 80);
		frmPractica.getContentPane().add(lblN);

		JLabel lblM = new JLabel("M");
		lblM.setOpaque(true);
		lblM.setHorizontalAlignment(SwingConstants.CENTER);
		lblM.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblM.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblM.setBounds(923, 786, 80, 80);
		frmPractica.getContentPane().add(lblM);

		JLabel label_8 = new JLabel(",");
		label_8.setOpaque(true);
		label_8.setHorizontalAlignment(SwingConstants.CENTER);
		label_8.setFont(new Font("Tahoma", Font.BOLD, 20));
		label_8.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_8.setBounds(1013, 786, 80, 80);
		frmPractica.getContentPane().add(label_8);

		JLabel label_9 = new JLabel(".");
		label_9.setOpaque(true);
		label_9.setHorizontalAlignment(SwingConstants.CENTER);
		label_9.setFont(new Font("Tahoma", Font.BOLD, 20));
		label_9.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_9.setBounds(1103, 786, 80, 80);
		frmPractica.getContentPane().add(label_9);

		JLabel label_11 = new JLabel("-");
		label_11.setOpaque(true);
		label_11.setHorizontalAlignment(SwingConstants.CENTER);
		label_11.setFont(new Font("Tahoma", Font.BOLD, 20));
		label_11.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_11.setBounds(1192, 786, 80, 80);
		frmPractica.getContentPane().add(label_11);

		JLabel lblSPACE = new JLabel("");
		lblSPACE.setOpaque(true);
		lblSPACE.setHorizontalAlignment(SwingConstants.CENTER);
		lblSPACE.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblSPACE.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblSPACE.setBounds(549, 877, 487, 80);
		frmPractica.getContentPane().add(lblSPACE);

		JLabel lbl1 = new JLabel("1");
		lbl1.setOpaque(true);
		lbl1.setHorizontalAlignment(SwingConstants.CENTER);
		lbl1.setFont(new Font("Tahoma", Font.BOLD, 20));
		lbl1.setBorder(new LineBorder(new Color(0, 0, 0)));
		lbl1.setBackground(SystemColor.menu);
		lbl1.setBounds(292, 514, 80, 80);
		frmPractica.getContentPane().add(lbl1);

		JLabel lbl2 = new JLabel("2");
		lbl2.setOpaque(true);
		lbl2.setHorizontalAlignment(SwingConstants.CENTER);
		lbl2.setFont(new Font("Tahoma", Font.BOLD, 20));
		lbl2.setBorder(new LineBorder(new Color(0, 0, 0)));
		lbl2.setBackground(SystemColor.menu);
		lbl2.setBounds(382, 514, 80, 80);
		frmPractica.getContentPane().add(lbl2);

		JLabel lbl3 = new JLabel("3");
		lbl3.setOpaque(true);
		lbl3.setHorizontalAlignment(SwingConstants.CENTER);
		lbl3.setFont(new Font("Tahoma", Font.BOLD, 20));
		lbl3.setBorder(new LineBorder(new Color(0, 0, 0)));
		lbl3.setBackground(SystemColor.menu);
		lbl3.setBounds(472, 514, 80, 80);
		frmPractica.getContentPane().add(lbl3);

		JLabel lbl4 = new JLabel("4");
		lbl4.setOpaque(true);
		lbl4.setHorizontalAlignment(SwingConstants.CENTER);
		lbl4.setFont(new Font("Tahoma", Font.BOLD, 20));
		lbl4.setBorder(new LineBorder(new Color(0, 0, 0)));
		lbl4.setBackground(SystemColor.menu);
		lbl4.setBounds(562, 514, 80, 80);
		frmPractica.getContentPane().add(lbl4);

		JLabel lbl5 = new JLabel("5");
		lbl5.setOpaque(true);
		lbl5.setHorizontalAlignment(SwingConstants.CENTER);
		lbl5.setFont(new Font("Tahoma", Font.BOLD, 20));
		lbl5.setBorder(new LineBorder(new Color(0, 0, 0)));
		lbl5.setBackground(SystemColor.menu);
		lbl5.setBounds(652, 514, 80, 80);
		frmPractica.getContentPane().add(lbl5);

		JLabel lbl6 = new JLabel("6");
		lbl6.setOpaque(true);
		lbl6.setHorizontalAlignment(SwingConstants.CENTER);
		lbl6.setFont(new Font("Tahoma", Font.BOLD, 20));
		lbl6.setBorder(new LineBorder(new Color(0, 0, 0)));
		lbl6.setBackground(SystemColor.menu);
		lbl6.setBounds(742, 514, 80, 80);
		frmPractica.getContentPane().add(lbl6);

		JLabel lbl7 = new JLabel("7");
		lbl7.setOpaque(true);
		lbl7.setHorizontalAlignment(SwingConstants.CENTER);
		lbl7.setFont(new Font("Tahoma", Font.BOLD, 20));
		lbl7.setBorder(new LineBorder(new Color(0, 0, 0)));
		lbl7.setBackground(SystemColor.menu);
		lbl7.setBounds(832, 514, 80, 80);
		frmPractica.getContentPane().add(lbl7);

		JLabel lbl8 = new JLabel("8");
		lbl8.setOpaque(true);
		lbl8.setHorizontalAlignment(SwingConstants.CENTER);
		lbl8.setFont(new Font("Tahoma", Font.BOLD, 20));
		lbl8.setBorder(new LineBorder(new Color(0, 0, 0)));
		lbl8.setBackground(SystemColor.menu);
		lbl8.setBounds(922, 514, 80, 80);
		frmPractica.getContentPane().add(lbl8);

		JLabel lbl9 = new JLabel("9");
		lbl9.setOpaque(true);
		lbl9.setHorizontalAlignment(SwingConstants.CENTER);
		lbl9.setFont(new Font("Tahoma", Font.BOLD, 20));
		lbl9.setBorder(new LineBorder(new Color(0, 0, 0)));
		lbl9.setBackground(SystemColor.menu);
		lbl9.setBounds(1012, 514, 80, 80);
		frmPractica.getContentPane().add(lbl9);

		JLabel lbl0 = new JLabel("0");
		lbl0.setOpaque(true);
		lbl0.setHorizontalAlignment(SwingConstants.CENTER);
		lbl0.setFont(new Font("Tahoma", Font.BOLD, 20));
		lbl0.setBorder(new LineBorder(new Color(0, 0, 0)));
		lbl0.setBackground(SystemColor.menu);
		lbl0.setBounds(1102, 514, 80, 80);
		frmPractica.getContentPane().add(lbl0);

		JLabel label_1 = new JLabel("\u00AA\r\n\u00BA \\\r\n");
		label_1.setOpaque(true);
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		label_1.setFont(new Font("Tahoma", Font.BOLD, 20));
		label_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_1.setBackground(SystemColor.menu);
		label_1.setBounds(202, 514, 80, 80);
		frmPractica.getContentPane().add(label_1);

		JLabel lblShift = new JLabel("TAB");
		lblShift.setOpaque(true);
		lblShift.setHorizontalAlignment(SwingConstants.CENTER);
		lblShift.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblShift.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblShift.setBackground(SystemColor.menu);
		lblShift.setBounds(202, 605, 124, 80);
		frmPractica.getContentPane().add(lblShift);

		JLabel lblBloqMayus = new JLabel("Bloq Mayus");
		lblBloqMayus.setOpaque(true);
		lblBloqMayus.setHorizontalAlignment(SwingConstants.CENTER);
		lblBloqMayus.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblBloqMayus.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblBloqMayus.setBackground(SystemColor.menu);
		lblBloqMayus.setBounds(202, 696, 134, 80);
		frmPractica.getContentPane().add(lblBloqMayus);

		JLabel lblShift_1 = new JLabel("SHIFT");
		lblShift_1.setOpaque(true);
		lblShift_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblShift_1.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblShift_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblShift_1.setBackground(SystemColor.menu);
		lblShift_1.setBounds(202, 787, 80, 80);
		frmPractica.getContentPane().add(lblShift_1);

		JLabel label_5 = new JLabel("< >");
		label_5.setOpaque(true);
		label_5.setHorizontalAlignment(SwingConstants.CENTER);
		label_5.setFont(new Font("Tahoma", Font.BOLD, 20));
		label_5.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_5.setBackground(SystemColor.menu);
		label_5.setBounds(292, 787, 80, 80);
		frmPractica.getContentPane().add(label_5);

		JLabel lblCtrl = new JLabel("Ctrl");
		lblCtrl.setOpaque(true);
		lblCtrl.setHorizontalAlignment(SwingConstants.CENTER);
		lblCtrl.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblCtrl.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblCtrl.setBackground(SystemColor.menu);
		lblCtrl.setBounds(202, 877, 98, 80);
		frmPractica.getContentPane().add(lblCtrl);

		JLabel lblWindows = new JLabel("Windows");
		lblWindows.setOpaque(true);
		lblWindows.setHorizontalAlignment(SwingConstants.CENTER);
		lblWindows.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblWindows.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblWindows.setBackground(SystemColor.menu);
		lblWindows.setBounds(310, 877, 116, 80);
		frmPractica.getContentPane().add(lblWindows);

		JLabel lblAlt = new JLabel("Alt");
		lblAlt.setOpaque(true);
		lblAlt.setHorizontalAlignment(SwingConstants.CENTER);
		lblAlt.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblAlt.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblAlt.setBackground(SystemColor.menu);
		lblAlt.setBounds(436, 877, 103, 80);
		frmPractica.getContentPane().add(lblAlt);

		JLabel lblAltGr = new JLabel("Alt Gr");
		lblAltGr.setOpaque(true);
		lblAltGr.setHorizontalAlignment(SwingConstants.CENTER);
		lblAltGr.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblAltGr.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblAltGr.setBackground(SystemColor.menu);
		lblAltGr.setBounds(1046, 877, 118, 80);
		frmPractica.getContentPane().add(lblAltGr);

		JLabel lblFn = new JLabel("Fn");
		lblFn.setOpaque(true);
		lblFn.setHorizontalAlignment(SwingConstants.CENTER);
		lblFn.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblFn.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblFn.setBackground(SystemColor.menu);
		lblFn.setBounds(1174, 877, 106, 80);
		frmPractica.getContentPane().add(lblFn);

		JLabel lblCtrl_1 = new JLabel("Ctrl");
		lblCtrl_1.setOpaque(true);
		lblCtrl_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblCtrl_1.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblCtrl_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblCtrl_1.setBackground(SystemColor.menu);
		lblCtrl_1.setBounds(1406, 877, 106, 80);
		frmPractica.getContentPane().add(lblCtrl_1);

		JLabel lblShift_2 = new JLabel("SHIFT");
		lblShift_2.setOpaque(true);
		lblShift_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblShift_2.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblShift_2.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblShift_2.setBackground(SystemColor.menu);
		lblShift_2.setBounds(1282, 786, 230, 80);
		frmPractica.getContentPane().add(lblShift_2);

		JLabel lblWindows_1 = new JLabel("Windows");
		lblWindows_1.setOpaque(true);
		lblWindows_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblWindows_1.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblWindows_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblWindows_1.setBackground(SystemColor.menu);
		lblWindows_1.setBounds(1290, 877, 106, 80);
		frmPractica.getContentPane().add(lblWindows_1);

		JLabel label_2 = new JLabel("\u00A8 \u00B4 {");
		label_2.setOpaque(true);
		label_2.setHorizontalAlignment(SwingConstants.CENTER);
		label_2.setFont(new Font("Tahoma", Font.BOLD, 20));
		label_2.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_2.setBounds(1246, 696, 80, 80);
		frmPractica.getContentPane().add(label_2);

		JLabel label_3 = new JLabel("\u00E7 }");
		label_3.setOpaque(true);
		label_3.setHorizontalAlignment(SwingConstants.CENTER);
		label_3.setFont(new Font("Tahoma", Font.BOLD, 20));
		label_3.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_3.setBounds(1336, 696, 80, 80);
		frmPractica.getContentPane().add(label_3);

		JLabel label_15 = new JLabel("");
		label_15.setOpaque(true);
		label_15.setHorizontalAlignment(SwingConstants.CENTER);
		label_15.setFont(new Font("Tahoma", Font.BOLD, 20));
		label_15.setBorder(null);
		label_15.setBounds(1419, 605, 23, 80);
		frmPractica.getContentPane().add(label_15);

		JLabel label_4 = new JLabel("");
		label_4.setOpaque(true);
		label_4.setHorizontalAlignment(SwingConstants.CENTER);
		label_4.setFont(new Font("Tahoma", Font.BOLD, 20));
		label_4.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_4.setBounds(1426, 605, 86, 171);
		frmPractica.getContentPane().add(label_4);

		JLabel label_6 = new JLabel("` ^ [");
		label_6.setOpaque(true);
		label_6.setHorizontalAlignment(SwingConstants.CENTER);
		label_6.setFont(new Font("Tahoma", Font.BOLD, 20));
		label_6.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_6.setBounds(1236, 605, 80, 80);
		frmPractica.getContentPane().add(label_6);

		JLabel label_7 = new JLabel("+ * ]");
		label_7.setOpaque(true);
		label_7.setHorizontalAlignment(SwingConstants.CENTER);
		label_7.setFont(new Font("Tahoma", Font.BOLD, 20));
		label_7.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_7.setBounds(1326, 605, 80, 80);
		frmPractica.getContentPane().add(label_7);

		JLabel label_12 = new JLabel("");
		label_12.setOpaque(true);
		label_12.setHorizontalAlignment(SwingConstants.CENTER);
		label_12.setFont(new Font("Tahoma", Font.BOLD, 20));
		label_12.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_12.setBounds(1416, 605, 96, 80);
		frmPractica.getContentPane().add(label_12);

		JLabel label_13 = new JLabel("' ?");
		label_13.setOpaque(true);
		label_13.setHorizontalAlignment(SwingConstants.CENTER);
		label_13.setFont(new Font("Tahoma", Font.BOLD, 20));
		label_13.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_13.setBackground(SystemColor.menu);
		label_13.setBounds(1192, 514, 80, 80);
		frmPractica.getContentPane().add(label_13);

		JLabel label_14 = new JLabel("\u00A1 \u00BF");
		label_14.setOpaque(true);
		label_14.setHorizontalAlignment(SwingConstants.CENTER);
		label_14.setFont(new Font("Tahoma", Font.BOLD, 20));
		label_14.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_14.setBackground(SystemColor.menu);
		label_14.setBounds(1282, 514, 80, 80);
		frmPractica.getContentPane().add(label_14);

		JLabel lblDelete = new JLabel("DELETE");
		lblDelete.setOpaque(true);
		lblDelete.setHorizontalAlignment(SwingConstants.CENTER);
		lblDelete.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblDelete.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblDelete.setBackground(SystemColor.menu);
		lblDelete.setBounds(1372, 514, 140, 80);
		frmPractica.getContentPane().add(lblDelete);

		JLabel lblPTotales = new JLabel("Pulsaciones totales:");
		lblPTotales.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblPTotales.setBounds(1350, 148, 198, 50);
		frmPractica.getContentPane().add(lblPTotales);

		lblPTotalesRes.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblPTotalesRes.setBounds(1652, 148, 80, 50);
		frmPractica.getContentPane().add(lblPTotalesRes);

		JLabel lblPPM = new JLabel("Pulsaciones por minuto:");
		lblPPM.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblPPM.setBounds(1350, 209, 198, 50);
		frmPractica.getContentPane().add(lblPPM);

		lblPPMRes.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblPPMRes.setBounds(1652, 209, 80, 50);
		frmPractica.getContentPane().add(lblPPMRes);

		JLabel lblErrores = new JLabel("Errores:");
		lblErrores.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblErrores.setBounds(1350, 270, 198, 50);
		frmPractica.getContentPane().add(lblErrores);

		lblErroresRes.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblErroresRes.setBounds(1652, 270, 80, 50);
		frmPractica.getContentPane().add(lblErroresRes);

		JLabel lblTiempo = new JLabel("Tiempo");
		lblTiempo.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblTiempo.setBounds(1350, 331, 198, 50);
		frmPractica.getContentPane().add(lblTiempo);

		lblContador.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblContador.setBounds(1652, 320, 80, 69);
		frmPractica.getContentPane().add(lblContador);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblNewLabel.setBounds(1336, 131, 506, 339);
		frmPractica.getContentPane().add(lblNewLabel);
		
		btnVolverMenu.setVisible(false);
		btnVolverMenu.setFocusPainted(false);
		btnVolverMenu.setBackground(new Color(176, 196, 222));
		btnVolverMenu.setFont(new Font("Tahoma", Font.BOLD, 16));
		btnVolverMenu.setBounds(1606, 888, 217, 69);
		frmPractica.getContentPane().add(btnVolverMenu);
		
		btnReintentarLeccion.setVisible(false);
		btnReintentarLeccion.setFocusPainted(false);
		btnReintentarLeccion.setFont(new Font("Tahoma", Font.BOLD, 16));
		btnReintentarLeccion.setBackground(new Color(176, 196, 222));
		btnReintentarLeccion.setBounds(1606, 786, 217, 69);
		frmPractica.getContentPane().add(btnReintentarLeccion);
		
		btnVolverMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				GuardarEstadisticas();
		        
		        menu cargar = new menu(Usuario);
				cargar.getFrameMenu().setVisible(true);
				frmPractica.dispose();
			}
		});
		
		btnReintentarLeccion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				GuardarEstadisticas();
		        
				practica cargar = new practica(Leccion, "Leccion 1", Usuario);
				cargar.getFrmPractica().setVisible(true);
				frmPractica.dispose();
			}
		});

		textPane.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_Q) {
					if (contadorletras == 0)
						Resaltar('Q', lblQ);
					else
						Resaltar('q', lblQ);
				}

				if (e.getKeyCode() == KeyEvent.VK_W) {
					if (contadorletras == 0)
						Resaltar('W', lblW);
					else
						Resaltar('w', lblW);
				}

				if (e.getKeyCode() == KeyEvent.VK_E) {
					if (contadorletras == 0)
						Resaltar('E', lblE);
					else
						Resaltar('e', lblE);
				}

				if (e.getKeyCode() == KeyEvent.VK_R) {
					if (contadorletras == 0)
						Resaltar('R', lblR);
					else
						Resaltar('r', lblR);
				}

				if (e.getKeyCode() == KeyEvent.VK_T) {
					if (contadorletras == 0)
						Resaltar('T', lblT);
					else
						Resaltar('t', lblT);
				}

				if (e.getKeyCode() == KeyEvent.VK_R) {
					if (contadorletras == 0)
						Resaltar('R', lblR);
					else
						Resaltar('r', lblR);
				}

				if (e.getKeyCode() == KeyEvent.VK_Y) {
					if (contadorletras == 0)
						Resaltar('Y', lblY);
					else
						Resaltar('y', lblY);
				}

				if (e.getKeyCode() == KeyEvent.VK_U) {
					if (contadorletras == 0)
						Resaltar('U', lblU);
					else
						Resaltar('u', lblU);
				}

				if (e.getKeyCode() == KeyEvent.VK_I) {
					if (contadorletras == 0)
						Resaltar('I', lblI);
					else
						Resaltar('i', lblI);
				}

				if (e.getKeyCode() == KeyEvent.VK_O) {
					if (contadorletras == 0)
						Resaltar('O', lblO);
					else
						Resaltar('o', lblO);
				}

				if (e.getKeyCode() == KeyEvent.VK_P) {
					if (contadorletras == 0)
						Resaltar('P', lblP);
					else
						Resaltar('p', lblP);
				}

				if (e.getKeyCode() == KeyEvent.VK_A) {
					if (contadorletras == 0)
						Resaltar('A', lblA);
					else
						Resaltar('a', lblA);
				}

				if (e.getKeyCode() == KeyEvent.VK_S) {
					if (contadorletras == 0)
						Resaltar('S', lblS);
					else
						Resaltar('s', lblS);
				}

				if (e.getKeyCode() == KeyEvent.VK_D) {
					if (contadorletras == 0)
						Resaltar('D', lblD);
					else
						Resaltar('d', lblD);
				}

				if (e.getKeyCode() == KeyEvent.VK_F) {
					if (contadorletras == 0)
						Resaltar('F', lblF);
					else
						Resaltar('f', lblF);
				}

				if (e.getKeyCode() == KeyEvent.VK_G) {
					if (contadorletras == 0)
						Resaltar('G', lblG);
					else
						Resaltar('g', lblG);
				}

				if (e.getKeyCode() == KeyEvent.VK_H) {
					if (contadorletras == 0)
						Resaltar('H', lblH);
					else
						Resaltar('h', lblH);
				}

				if (e.getKeyCode() == KeyEvent.VK_J) {
					if (contadorletras == 0)
						Resaltar('J', lblJ);
					else
						Resaltar('j', lblJ);
				}

				if (e.getKeyCode() == KeyEvent.VK_K) {
					if (contadorletras == 0)
						Resaltar('K', lblK);
					else
						Resaltar('k', lblK);
				}

				if (e.getKeyCode() == KeyEvent.VK_L) {
					if (contadorletras == 0)
						Resaltar('L', lblL);
					else
						Resaltar('l', lblL);
				}

				if (e.getKeyCode() == KeyEvent.VK_Z) {
					if (contadorletras == 0)
						Resaltar('Z', lblZ);
					else
						Resaltar('z', lblZ);
				}

				if (e.getKeyCode() == KeyEvent.VK_X) {
					if (contadorletras == 0)
						Resaltar('X', lblX);
					else
						Resaltar('X', lblX);
				}

				if (e.getKeyCode() == KeyEvent.VK_C) {
					if (contadorletras == 0)
						Resaltar('C', lblC);
					else
						Resaltar('c', lblC);
				}

				if (e.getKeyCode() == KeyEvent.VK_V) {
					if (contadorletras == 0)
						Resaltar('V', lblV);
					else
						Resaltar('v', lblV);
				}

				if (e.getKeyCode() == KeyEvent.VK_B) {
					if (contadorletras == 0)
						Resaltar('B', lblB);
					else
						Resaltar('b', lblB);
				}

				if (e.getKeyCode() == KeyEvent.VK_N) {
					if (contadorletras == 0)
						Resaltar('N', lblN);
					else
						Resaltar('n', lblN);
				}

				if (e.getKeyCode() == KeyEvent.VK_M) {
					if (contadorletras == 0)
						Resaltar('m', lblM);
					else
						Resaltar('M', lblM);
				}

				if (e.getKeyCode() == KeyEvent.VK_1)
					Resaltar('1', lbl1);

				if (e.getKeyCode() == KeyEvent.VK_2)
					Resaltar('2', lbl2);

				if (e.getKeyCode() == KeyEvent.VK_3)
					Resaltar('3', lbl3);

				if (e.getKeyCode() == KeyEvent.VK_4)
					Resaltar('4', lbl4);

				if (e.getKeyCode() == KeyEvent.VK_4)
					Resaltar('4', lbl4);

				if (e.getKeyCode() == KeyEvent.VK_5)
					Resaltar('5', lbl5);

				if (e.getKeyCode() == KeyEvent.VK_6)
					Resaltar('6', lbl6);

				if (e.getKeyCode() == KeyEvent.VK_7)
					Resaltar('7', lbl7);

				if (e.getKeyCode() == KeyEvent.VK_8)
					Resaltar('8', lbl8);

				if (e.getKeyCode() == KeyEvent.VK_9)
					Resaltar('9', lbl9);

				if (e.getKeyCode() == KeyEvent.VK_0)
					Resaltar('0', lbl0);

				if (e.getKeyCode() == KeyEvent.VK_SPACE)
					Resaltar(' ', lblSPACE);
			}
		});
	}
	
	public void GuardarEstadisticas() {
		FileWriter fichero = null;
        PrintWriter pw = null;
        try
        {
        	String estadisticas = "file\\" + Usuario + "estadisticas.txt";
            fichero = new FileWriter(estadisticas, true);
            pw = new PrintWriter(fichero);

            //for (int i = 0; i < 10; i++)
                pw.println("\nLeccion: " + NumeroLeccion + "\nFecha: " + fecha + "\nPulsaciones totales: " + pTotales + "\nPulsaciones por minuto: " + ppm + "\nErrores: " + errores + "\nTiempo: " + min + ":" + sec);

        } catch (Exception er) {
            er.printStackTrace();
        } finally {
           try {
           // Nuevamente aprovechamos el finally para 
           // asegurarnos que se cierra el fichero.
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }
	}

	public void Contador(JLabel lblContador) {
		// Aqu� se pone en marcha el timer cada segundo.
		timer = new Timer(1000, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				sec++;

				if(stoplec)
					timer.stop();
				
				if (sec == 60) {
					min += 1;
					sec = 0;
				}

				if (sec < 10)
					lblContador.setText("0" + min + ":" + "0" + sec);
				else
					lblContador.setText("0" + min + ":" + sec);
				if (min == 5) {
					timer.stop();
				}

				pTotales = Integer.toString(contadorletras);
				lblPTotalesRes.setText(pTotales);

				ppm = String.valueOf((60 * Integer.parseInt(pTotales)) / sec);

				lblPPMRes.setText(ppm);

			}
		});

		timer.start();
	}

	public void Resaltar(char letrapulsada, JLabel label) {

		if(aCaracteres.length == contadorletras) {
			stoplec = true;
			JOptionPane.showMessageDialog(null, "Enhorabuena! Has terminado tu leccion.");
			btnReintentarLeccion.setVisible(true);
			btnVolverMenu.setVisible(true);
			
		}
		
		if (!start) {
			Contador(lblContador);
			start = true;
		}

		if (contadorletras != 0) 
			labelaux.setBackground(SystemColor.control);

		if (aCaracteres[contadorletras] == letrapulsada) {
			label.setBackground(Color.GREEN);
		} else {
			label.setBackground(Color.RED);
			errores++;
			String err = Integer.toString(errores);
			lblErroresRes.setText(err);
		}
		
		labelaux = label;
		contadorletras++;
	}
}
