import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class leer_archivo {
    
    public String muestraContenido(String archivo, String opcion) throws FileNotFoundException, IOException {
        String cadena = "", 
        		texto = "",
        		practica = "";
        BufferedReader b = new BufferedReader(new InputStreamReader(new FileInputStream(archivo), "utf-8"));
        while((cadena = b.readLine())!=null) {
        	texto += cadena;
        }
        b.close();
                
        practica = leccion(texto, opcion);
        
        return practica;
    }
    
    public String leccion(String texto, String opcion) {
    	
    	String buscar = "";
    	
    	if(opcion == "Leccion 1") {
    		opcion = "Leccion 2";
    		buscar = "Leccion 1";
    	}
    	else if (opcion == "Leccion 2") {
			opcion = "Leccion 3";
    		buscar = "Leccion 2";
    	}

        int inicio = texto.indexOf(buscar);
        int fin = texto.indexOf(opcion, inicio + 9);

        String leccion = texto.substring(inicio + 9, fin);
        
        return leccion;
    }

    
    public boolean encuentra(String fichero, String pass, String usuario, boolean login) throws FileNotFoundException {
    	
        String linea="";
        try {
        BufferedReader br=new BufferedReader(new FileReader(fichero));

        while ((linea= br.readLine())!=null) {  	
        	if(linea.equals(usuario + " " + pass))
        		login = true;	
        	else if (!login)
                login = false;
         }
         br.close();
        } catch (IOException e) {
            System.out.println("Error en el archivo");
        }   
        
        return login;
    }
   
}